const express= require('express');

const router = express.Router();

const routeuser = require('./routes/user');
const routerole = require('./routes/role');
const routeauth = require('./routes/auth');
const routearticle = require('./routes/article');
const routecomment = require('./routes/comment');
const routeadoption = require('./routes/adoption');


router.use('/user',routeuser);
router.use('/role',routerole);
router.use('/auth',routeauth);
router.use('/article',routearticle);
router.use('/comment',routecomment);
router.use('/adoption',routeadoption);

module.exports = router;