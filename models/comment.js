'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentSchema = Schema({
    text: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now,
        required: true
    },
    article: {
        type: Schema.ObjectId,
        ref: 'Article',
        required: true
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    vote:[{
        user: {
            type: Schema.ObjectId,
            ref: 'User'
        },
        vote: {
            type: Number
        }
    }]



});

module.exports = mongoose.model('Comment', commentSchema);