'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profileSchema = Schema({
    name: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    animal: {
        type: String,
        required: true
    },
    sex: {
        type: String,
        required: true
    },
    cover: {
        type: String,
        required: false
    },
    date: {
        type: Date, default: Date.now
    },
    media: [{
        type: String,
        required: true
    }],
    adopted: {
        type: Number,
        required: true,
        default: 0
    },

    adopterData: {
        name: {
            type: String
        },
        email: {
            type: String
        },
        text: {
            type: String
        },
        required: false
    },

    user: {
        type: Schema.ObjectId, ref: 'User'
    },

});

module.exports = mongoose.model('Adoption', profileSchema);