'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoleSchema = Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    permissions: {
        type: Number,
        required: true
    },
    icon: {
        type: String,
        required: true
    },
    special: {
        type: Number,
        required: true,
        default: 0
    }
});

const Role = mongoose.model('Role', RoleSchema);

module.exports = Role;