'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = Schema({
    name: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    fb_id:{
        type: String,
        required: false,
        select: false
    },
    image: {
        type: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now,
        required: true
    },

    role: { type: Schema.ObjectId,
        ref: 'Role',
        required: false
    }
});

UserSchema.pre('remove', function(next) {
    const that = this;
    this.model('Article').find({ user: this._id }, async function(err,articles){
        for(let index in articles){
            //Tengo que hacerlo así para que se borren también los comentarios
            await articles[index].remove();
        }
        that.model('Comment').remove({ user: that._id }).exec();
        that.model('Adoption').remove({ user: that._id }).exec();
        //Borro todos los votos que ha realizado este usuario
        that.model('Comment').update({},{$pull: {vote: {user:that._id }}}, { multi: true }).exec();
        next();
    });
});


module.exports = mongoose.model('User', UserSchema);