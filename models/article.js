'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ArticleSchema = Schema({
    text: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now,
        required: true
    },
    tags: [{
        type: String,
        required: true
    }],

    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
});


ArticleSchema.pre('remove', function(next) {
    this.model('Comment').remove({ article: this._id }).exec();
    next();
});

module.exports = mongoose.model('Article', ArticleSchema);