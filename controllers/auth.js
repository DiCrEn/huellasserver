'use strict'

const User = require('../models/user');
const encryptutils = require("../utils/encryptutils");
const axios = require('axios');
const config = require('config');

module.exports = {

    facebook(req, res, next){

        //Para probar desde servidor
        //https://www.facebook.com/v3.0/dialog/oauth?response_type=code&client_id=241575486399442&redirect_uri=https://huellas.dicren.com/api/v1/auth/facebook/&display=popup&scope=email
        //Para probar desde local
        //https://www.facebook.com/v3.0/dialog/oauth?response_type=code&client_id=241575486399442&redirect_uri=http://localhost:8888/api/v1/auth/facebook/&display=popup&scope=email

        const code = req.query.code;
        //redirect_uri="https://dicren.com:8088/api/v1/auth/facebook/";
        const redirect_uri=config.get("fb_redirect_url");

        axios.post('https://graph.facebook.com/v3.0/oauth/access_token', {
            client_id: config.get("fb_client_id"),
            client_secret: config.get("fb_client_secret"),
            code: code,
            redirect_uri: redirect_uri
        }).then(async function (response) {

            try {
                const access_token = response.data.access_token;

                const userData = await axios.get('https://graph.facebook.com/v3.0/me', {
                    params: {
                        access_token: access_token,
                        fields: 'email, first_name, last_name, id, picture.type(large)'
                    }
                });

                let myUserData = await User.findOne({"fb_id": userData.data.id}).populate({path: 'role'}).exec();

                let finalresponse;

                if (myUserData) {
                    //Devuelvo los datos como un login normal
                    myUserData = myUserData.toObject();
                    myUserData.token = encryptutils.createToken(myUserData._id);
                    finalresponse = myUserData;
                } else {
                    //Envio los datos a la interface para rellenar el formulario de registro.
                    finalresponse = {
                        fb_user: userData.data
                    };
                }

                res.json(finalresponse);

            }catch(e){
                console.log(e);
                let err = new Error("Error al obtener los datos de facebook");
                err.status = 500;
                next(err);
            }


        }).catch(function (errror) {
            console.log(errror);
            let err = new Error(JSON.stringify(errror));
            err.status = 500;
            next(err);
        })
    },

};