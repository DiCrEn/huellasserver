'use strict'

const User = require('../models/user');
const Role = require('../models/role');
const Comment = require('../models/comment');
const errorutils = require("../utils/errorsutils");

module.exports = {

    async newComment(req, res, next) {

        const comment = new Comment();

        comment.text = req.body.text;
        comment.article = req.body.article;
        comment.user = req.user;

        try{
            const commentI = await comment.save();
            await commentI.populate(
                {path: 'user', model: 'User', populate:
                        {
                            path: 'role',
                            model: 'Role'
                        }
                }).execPopulate();

            res.json(commentI);
        }catch(error){
            const txterr = errorutils.mongooseErrors(error);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },

    async getComment(req,res,next){

        const limit = parseInt(req.query.perpage) || 10;
        const page = parseInt(req.query.page)-1 || 0;
        const skip = page * (limit);
        const orderby = req.query.orderby || "date";
        const order = req.query.order || -1;
        const count = req.query.count || 0;

        let find = {};
        if(req.params.id) find = {_id: req.params.id};

        if(count){
            let count = await Comment.count().exec();
            res.json(count);
        }else {

            try {
                let comments = await Comment.find(find).sort([[orderby, order]]).skip(skip).limit(limit).populate('user article').exec();
                res.json(comments);

            } catch (error) {
                const txterr = errorutils.mongooseErrors(error);
                let err = new Error(txterr.txt);
                err.status = txterr.code;
                next(err);
            }
        }

    },

    async editComment(req,res,next){

        let id = req.params.id;
        try{
            const comment = await Comment.findByIdAndUpdate(id, {text: req.body.text}, {new: true, runValidators: true}).populate('user vote.user').exec();
            res.json(comment);
        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },

    async deleteComment(req,res,next){

        let id = req.params.id;
        try{
            const comment = await Comment.findByIdAndRemove(id).exec();
            res.json(comment);
        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },

    async voteComment(req,res,next){

        let id = req.params.id;
        const vote = req.body.vote;
        const data = {
            user: req.user,
            vote: vote > 0 ? 1 : -1 //Solo puedo votar por un punto
        };

        try{
            let comment = await Comment.findOne({_id: id, 'vote.user': req.user }).exec();

            if(comment) {
                let err = new Error('Ya has votado ese comentario');
                err.status = 409;
                next(err);
            }
            let comment2 = await Comment.findOne({_id: id}).exec();
            if(comment2.user.toString() === req.user.toString()) {
                let err = new Error('No puedes votar sobre tus propios comentarios');
                err.status = 409;
                next(err);
            }else{
                comment = await Comment.findByIdAndUpdate(id, {$push: {vote: data}}, {
                    new: true,
                    runValidators: true
                }).populate('user vote.user').exec();
                res.json(comment);
            }

        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    }


};