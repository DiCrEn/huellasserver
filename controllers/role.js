'use strict'

const User = require('../models/user');
const Role = require('../models/role');
const errorutils = require("../utils/errorsutils");
const databaseutils = require("../utils/databaseutils");

module.exports = {

    insertRole(req, res, next) {

        const role = new Role();
        role.name = req.body.name;
        role.permissions = req.body.permissions;
        role.icon = req.body.icon;

        role.save(async (error,role) => {
            if (error) {
                const txtError = errorutils.mongooseErrors(error);
                if(txtError.code === 11000){
                    let err = new Error("Ya existe ese rol");
                    err.status = 409;
                    next(err);
                }else {
                    let err = new Error(txtError.txt);
                    err.status = txtError.code;
                    next(err);
                }
            }else if(!role){
                let err = new Error('No se ha registrado el rol');
                err.status = 500;
                next(err);
            }else{
                role = await databaseutils.reversePopulation(role, User, 'role', "users", {} , "count");
                res.json(role);
            }
        });

    },
    async editRole(req,res,next){

        let idRol = req.params.id;
        let update = req.body;

        try {
            const rolechk = await Role.findById(idRol).exec();
            if(rolechk.special === 1 && update.permissions){
                let err = new Error("No se puede cambiar los permisos al rol administrador.");
                err.status = 422;
                next(err);
            }else {
                let roledit = await Role.findByIdAndUpdate(idRol, update, {new: true, runValidators: true}).exec();
                roledit = await databaseutils.reversePopulation(roledit, User, 'role', "users", {} , "count");
                res.json(roledit);
            }
        }catch(e){
            const txtError = errorutils.mongooseErrors(e);
            let err = new Error(txtError.txt);
            err.status = txtError.code;
            next(err);
        }


    },
    async getRoles(req,res,next){

        let find = {};
        if(req.params.id) {
            find = {
                _id: req.params.id
            };
        }
        try{
            let roles = await Role.find(find).exec();
            //roles = await databaseutils.reversePopulation(roles,User,"role","users","-email");
            roles = await databaseutils.reversePopulation(roles,User,'role',"users", {} ,"count");

            if(req.params.id) {
                if(roles[0]) {
                    res.json(roles[0]);
                }else{
                    let err = new Error('No encontrado');
                    err.status = 404;
                    next(err);
                }
            }else {
                res.json(roles);
            }

        }catch(e){
            const txtError = errorutils.mongooseErrors(e);
            let err = new Error(txtError.txt);
            err.status = txtError.code;
            next(err);
        }

    },

    async deleteRole(req,res,next){

        try {
            const users = await User.find({"role":req.params.id}).exec();
            if(users.length > 0) {
                let err = new Error("No se puede borrar el rol, aún hay usuarios con este rol.");
                err.status = 422;
                next(err);
            }else {
                const rolechk = await Role.findById(req.params.id).exec();
                if(!rolechk){
                    let err = new Error('No encontrado');
                    err.status = 404;
                    next(err);
                }else {
                    if (rolechk.special !== 0) {
                        let err = new Error("No se puede borrar los roles especiales.");
                        err.status = 422;
                        next(err);
                    } else {
                        const role = await Role.findOneAndRemove({_id: req.params.id}).exec();
                        res.json(role);
                    }
                }
            }
        } catch (e) {
            const txtError = errorutils.mongooseErrors(e);
            let err = new Error(txtError.txt);
            err.status = txtError.code;
            next(err);
        }

    }


};