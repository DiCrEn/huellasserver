'use strict'

const User = require('../models/user');
const Role = require('../models/role');
const Comment = require('../models/comment');
const Article = require('../models/article');
const Adoption = require('../models/adoption');

const encryptutils = require("../utils/encryptutils");
const errorutils = require("../utils/errorsutils");
const roleutils = require("../utils/roleutils");
const fs = require('fs');
const path = require('path');
const https = require('https');
const databaseutils = require("../utils/databaseutils");
const sharp = require('sharp');
const mongoose = require('mongoose');


module.exports = {

    /* ADD */
    /* Añadir un usuario a la base de datos */
    async insertUser(req, res, next) {

        const user = new User();

        if(!req.body.password){
            let err = new Error("El campo contraseña es obligatorio.");
            err.status = 400;
            return next(err);
        }

        user.name = req.body.name;
        user.lastname = req.body.lastname;
        user.email = req.body.email.toLowerCase();
        user.password = encryptutils.encryptpass(req.body.password);
        user.fb_id = req.body.fb_id;

        if(req.body.image) {
            const namefile = "fb_image_" + Math.random().toString(36).substring(7) + ".jpg";
            const file = fs.createWriteStream("./uploads/users/avatars/" + namefile);
            await new Promise((resolve, reject) => {
                https.get(req.body.image, function (response) {
                    try {
                        response.pipe(file);
                        user.image = namefile;
                        resolve();
                    } catch (e) {
                        reject(e);
                    }
                });
            });
        }

        if(req.body.role){
            //Tengo permisos para asignar roles a usuarios?
            if(roleutils.isPermitted(req.permissions, roleutils.ROLE_LIST.user.e)){
                user.role = req.body.role;
            }else{
                //error
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                next(err);
                return;
            }
        }else{
            //Asignar el role de usuario
            const role = await Role.findOne({special:2}).exec();
            user.role = role._id;
        }

        user.save((error, userStored) => {
            if (error) {
                const txtError = errorutils.mongooseErrors(error);
                let err = new Error(txtError.txt);
                err.status = txtError.code;
                next(err);
            } else {
                if (!userStored) {
                    let err = new Error('No se ha registrado el usuario');
                    err.status = 500;
                    next(err);
                } else {
                    User.populate(userStored, {path:"role"}, function(error,userPopulated){
                        if(error){
                            const txtError = errorutils.mongooseErrors(error);
                            let err = new Error(txtError.txt);
                            err.status = txtError.code;
                            next(err);
                        }else {
                            let user = userPopulated.toObject();
                            user.password = undefined;
                            if (!res.user) { //Si me estoy registrando yo ya envio el token para login
                                user.token = encryptutils.createToken(user._id);
                            }
                            res.json(user);
                        }
                    });
                }
            }
        });
    },

    /* EDIT */
    editUser(req,res,next){

        let id = req.params.id;
        let update = req.body;

        if (update.password) {
            update.password = encryptutils.encryptpass(update.password);
        }

        if(req.body.role){
            //Tengo permisos para asignar roles a usuarios?
            if(!roleutils.isPermitted(req.permissions, roleutils.ROLE_LIST.user.e)){
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                return next(err);
            }
        }

        //Con runValidators no borra los campos required, pero no lanza errores si no envio el campo
        //Sin embargo, si lo envio vacio (definido pero un array vacio) si que lanza error.
        User.findByIdAndUpdate(id, update, {new: true, runValidators: true}).populate({path: 'role'}).exec((error, user) => {

            if (error) {
                const txtError = errorutils.mongooseErrors(error);
                let err = new Error(txtError.txt);
                err.status = txtError.code;
                next(err);
            } else {
                user = user.toObject();
                user.password = undefined;
                if(user._id.toString() === req.user.toString()){
                    //Estoy editado mi propio usuario, tengo que recargar el token
                    user.token = encryptutils.createToken(user._id);
                }
                res.json(user);
            }
        });

    },

    /* SELECT */
    async getUsers(req,res,next){

        let email = null;

        //Las selecciones de la búsqueda o por defecto
        const search = req.query.search || "";
        const orderby = req.query.orderby || "date";
        const order = req.query.order || -1;

        const limit = parseInt(req.query.perpage) || 10;
        const page = parseInt(req.query.page)-1 || 0;
        const skip = page * (limit);

        //Si soy admin puedo obtener más campos.
        if(roleutils.isPermitted(req.permissions,roleutils.ROLE_LIST.user.r)){
            email = true;
        }

        const totalCount = await User.count({ $or: [{
            name: {
                $regex: search,
                    $options: "i"
            }},{
            lastname: {
                $regex: search,
                $options: "i"
            }}]
        });

        if(req.params.id){
            if(req.user && req.params.id.toString() === req.user.toString()){
                email = true; //Sin mis propios datos.
            }
        }

        let pipeline = [
            {
                $lookup: {
                    from: "comments",
                    localField: "_id",
                    foreignField: "user",
                    as: "comments"
                }
            },
            {
                $lookup: {
                    from: "articles",
                    localField: "_id",
                    foreignField: "user",
                    as: "articles"
                }
            },
            {
                $project:{
                    _id: true,
                    name: true,
                    lastname: true,
                    email: email,
                    image: true,
                    date: true,
                    role: true,
                    numcomments: {
                        $size: "$comments"
                    },
                    numarticles: {
                        $size: "$articles"
                    },
                    fullname:{
                        $concat: ["$name"," ","$lastname"]
                    }
                }
            },
            {
                $sort: {
                    [orderby]: parseInt(order)
                }
            },
            {
                $match:{
                    fullname: {
                        $regex: search,
                        $options: "i"
                    }
                }
            }


        ];

        try {

            if (req.params.id) {
                pipeline.unshift({$match: {_id: mongoose.Types.ObjectId(req.params.id)}});
            }

        }catch(e){
            const txterr2 = errorutils.mongooseErrors(e);
            let err2 = new Error(txterr2.txt);
            err2.status = txterr2.code;
            return next(err2);
        }

        try {

            let users = await User.aggregate(pipeline).skip(skip).limit(limit).exec();

            //Pueblo los roles
            await Role.populate(users,{path: "role"});


            users = await databaseutils.reversePopulation(users, Comment, 'user', "lastcomment", {
                sort: {date: 'desc'},
                select: 'date',
                limit: 1
            });
            users = await databaseutils.reversePopulation(users, Article, 'user', "lastarticle", {
                sort: {date: 'desc'},
                select: 'date',
                limit: 1
            });
            users = await databaseutils.reversePopulation(users, Adoption, 'user', "lastadoption", {
                sort: {date: 'desc'},
                select: 'date',
                limit: 1
            });

            users = await databaseutils.reversePopulation(users, Comment, 'user', "numcomments", {}, "count");
            users = await databaseutils.reversePopulation(users, Article, 'user', "numarticles", {}, "count");
            users = await databaseutils.reversePopulation(users, Adoption, 'user', "numadoptions", {}, "count");


            for (let index in users) {
                const registerDate = users[index].date;
                let dates = [];
                dates.push(new Date(users[index].lastcomment[0] ? users[index].lastcomment[0].date : registerDate));
                dates.push(new Date(users[index].lastarticle[0] ? users[index].lastarticle[0].date : registerDate));
                dates.push(new Date(users[index].lastadoption[0] ? users[index].lastadoption[0].date : registerDate));
                users[index].lastActivity = new Date(Math.max.apply(null, dates));
                users[index].lastcomment = undefined;
                users[index].lastarticle = undefined;
                users[index].lastadoption = undefined;
            }

            if (req.params.id) {
                if (users[0]) {
                    //Ultimos 5 articulos y roles.
                    users = await databaseutils.reversePopulation(users, Comment, 'user', "lastcomments", {
                        populate: 'user article',
                        sort: {date: 'desc'},
                        limit: 5
                    });
                    users = await databaseutils.reversePopulation(users, Article, 'user', "lastarticles", {
                        populate: {path: 'user', model: 'User'},
                        sort: {date: 'desc'},
                        limit: 5,
                        functionPerData: async function(data){
                            let returned = [];
                            for(let index in data){
                                let obj = data[index].toObject();
                                obj["numcomments"] = await Comment.find({article: obj._id}).count();
                                returned.push(obj);
                            }

                            return returned;
                        }
                    });

                    res.json(users[0]);
                } else {
                    let err = new Error('No encontrado');
                    err.status = 404;
                    next(err);
                }
            } else {

                const result = {
                    "totalRecords": totalCount,
                    "page": page+1,
                    "result": users
                };

                res.json(result);
            }

        }catch(e) {
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }


    },

    deleteUser(req,res,next){

        User.findById(req.params.id).exec(async (error,user) => {
            if (error) {
                const txterr = errorutils.mongooseErrors(error);
                let err = new Error(txterr.txt);
                err.status = txterr.code;
                next(err);
            }else{
                if(user.image && user.image !== ""){
                    fs.exists('./uploads/users/avatars/' + user.image, function (exist) {
                        if(exist)
                            fs.unlinkSync('./uploads/users/avatars/' + user.image);
                    });
                }
                await user.remove();

                res.json(user);
            }
        });


    },


    /* Loguea un usuario, devuelve un objeto con el usuario y el token*/
    login(req,res,next){

        if(!req.body.password){
            let err = new Error("El campo contraseña es obligatorio.");
            err.status = 400;
            return next(err);
        }

        const email = req.body.email;
        const password = req.body.password;

        User.findOne({email: email.toLowerCase()}).select('+password').populate({path: 'role'}).exec(function (error,user){
            if (error) {
                const txtError = errorutils.mongooseErrors(error);
                let err = new Error(txtError.txt);
                err.status = txtError.code;
                next(err);
            }else{
                if(user && user.password === encryptutils.encryptpass(password)){
                    user = user.toObject();
                    user.password = undefined;
                    user.token = encryptutils.createToken(user._id);
                    res.json(user);

                }else{
                    let err = new Error('Email o contraseña inválidos');
                    err.status = 403;
                    next(err);
                }
            }

        });

    },

    async setAvatar(req, res, next){

        const userid = req.params.id;
        if(req.files){

            const path = req.files.image.path;

            const split = path.split("/");
            let name = split[split.length-1];

            const extsplit = name.split('\.');
            const ext = extsplit[extsplit.length-1];
            extsplit.splice(-1,1);
            name = extsplit.join();
            split.splice(-1,1);
            const route = split.join('/');

            if(ext === 'png' || ext === 'jpg' || ext === 'jpeg' || ext === 'gif'){

                let user = await User.findById(userid).exec();
                if(user.image && user.image !== ""){
                    fs.exists('./uploads/users/avatars/' + user.image, function (exist) {
                        if(exist)
                            fs.unlinkSync('./uploads/users/avatars/' + user.image);
                    });
                }

                const nameEdit = route+"/"+name+'LOW.'+ext;
                sharp(path)
                    .resize(150,150)
                    .crop()
                    .withoutEnlargement(true)
                    .jpeg({
                        quality: 50,
                        progresive: true,
                        chromaSubsampling: '4:4:4',
                        optimiseScans: true
                    })
                    .toFile(nameEdit).then(async function(){

                    fs.unlinkSync(path);
                    user = await User.findByIdAndUpdate(userid, {image: name+'LOW.'+ext}, {new: true}).exec();
                    res.json(user);

                });

            }else{
                fs.unlinkSync(path);
                let err = new Error('Tipo de fichero no soportado');
                err.status = 415;
                next(err);
            }

        }else{
            let err = new Error('No se ha subido ninguna imagen');
            err.status = 403;
            next(err);
        }

    },

    async deleteAvatar(req, res, next){

        const id = req.params.id;
        try {
            let user = await User.findByIdAndUpdate(id, {image: ""}, {new: false}).exec();
            if(!user){
                let err = new Error('No se encuentra el usuario');
                err.status = 404;
                next(err);
            }else {
                try {
                    fs.unlinkSync('./uploads/users/avatars/' + user.image);
                }catch(e){}
                user = user.toObject();
                user.image = "";
                res.json(user);
            }
        }catch(e){
            const txtError = errorutils.mongooseErrors(e);
            let err = new Error(txtError.txt);
            err.status = txtError.code;
            next(err);
        }

    },
    async getAvatar(req, res, next){

        const id = req.params.id;
        const pathfile = './uploads/users/avatars/' + id;

        fs.exists(pathfile, function (exist) {
            if (exist) {
                res.sendFile(path.resolve(pathfile));
            } else {
                let err = new Error('No se encuentra la imagen');
                err.status = 404;
                next(err);
            }
        });
    }




};