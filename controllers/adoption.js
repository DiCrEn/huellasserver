'use strict'

const errorutils = require("../utils/errorsutils");
const Adoption = require('../models/adoption');
const sanitizehtml = require('sanitize-html');
const fs = require('fs');
const path = require('path');
const sharp = require('sharp');
const Email = require('email-templates');
const config = require('config');


//https://github.com/punkave/sanitize-html
const sanitizecfg = {
    allowedTags: [ 'h1','h2','h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol','u',
        'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
        'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe', 'img' ],
    allowedAttributes: {
        a: [ 'href', 'name', 'target' ],
        div: [ 'style' ],
        img: [ 'src' ]
    },
    allowedStyles: {
        '*': {
            // Match HEX and RGB
            'color': [/^\#(0x)?[0-9a-f]+$/i, /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/],
            'text-align': [/^left$/, /^right$/, /^center$/],
            // Match any number with px, em, or %
            'font-size': [/^\d+(?:px|em|%)$/]
        }
    },

    selfClosing: [ 'img', 'br', 'hr', 'area', 'base', 'basefont', 'input', 'link', 'meta' ],

    allowedSchemes: [ 'http', 'https', 'ftp', 'mailto', 'data' ],
    allowedSchemesByTag: {},
    allowedSchemesAppliedToAttributes: [ 'href', 'src', 'cite' ],
    allowProtocolRelative: true,
    allowedIframeHostnames: ['www.youtube.com', 'player.vimeo.com']
};

module.exports = {

    async newAdoption(req,res,next){

        const adoption = new Adoption();

        adoption.name = req.body.name;
        adoption.text = sanitizehtml(req.body.text, sanitizecfg);
        adoption.animal = req.body.animal;
        adoption.sex = req.body.sex;
        adoption.user = req.user;
        adoption.media = req.body.media || [];

        try {
            const adoptionInserted = await adoption.save();
            res.json(adoptionInserted);
        }catch(error){
            const txterr = errorutils.mongooseErrors(error);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },
    async getAdoption(req,res,next){

        const orderby = req.query.orderby || "date";
        const order = req.query.order || -1;

        const adopted = req.query.adopted || 0;

        const limit = parseInt(req.query.perpage) || 10;
        const page = parseInt(req.query.page)-1 || 0;
        const skip = page * (limit);

        const count = req.query.count || 0;

        let find = {adopted: adopted};
        if (req.params.id) {
            find = {
                _id: req.params.id
            };
        }

        if(count) {
            let count = await Adoption.count(find).exec();
            res.json(count);
        }else {

            try {
                let adoption = await Adoption.find(find).skip(skip).limit(limit).sort([[orderby, order]]).populate("user adopter").exec();
                if (req.params.id) {
                    if (adoption[0]) {
                        res.json(adoption[0]);
                    } else {
                        let err = new Error('No encontrado');
                        err.status = 404;
                        next(err);
                    }
                } else {
                    res.json(adoption);
                }
            } catch (error) {
                const txterr = errorutils.mongooseErrors(error);
                let err = new Error(txterr.txt);
                err.status = txterr.code;
                next(err);
            }
        }
    },
    async editAdoption(req,res,next){


        let id = req.params.id;
        let update = req.body;
        update.text = sanitizehtml(update.text, sanitizecfg);

        const preedit = await Adoption.findById(id).exec();

        try {
            const adoption = await Adoption.findByIdAndUpdate(id, update, {new: true, runValidators: true}).populate("user adopter").exec();
            res.json(adoption);

            if(preedit.adopted === 1 && adoption.adopted === 2){

                const email = new Email({
                    message: {
                        from: '"Huellas" <'+config.get("email")+'>',
                    },
                    send: true,
                    transport: {
                        service: 'gmail',
                        auth: {
                            user: config.get("email"),
                            pass: config.get("email_pass")
                        }
                    }
                });

                email
                    .send({
                        template: 'adoptionok',
                        message: {
                            to: update.adopteremail
                        },
                        locals: {
                            name: adoption.name
                        }
                    })
                    .then()
                    .catch(console.error);


            }else if(preedit.adopted === 1 && adoption.adopted === 0){

                const email = new Email({
                    message: {
                        from: '"Huellas" <'+config.get("email")+'>',
                    },
                    send: true,
                    transport: {
                        service: 'gmail',
                        auth: {
                            user: config.get("email"),
                            pass: config.get("email_pass")
                        }
                    }
                });

                email
                    .send({
                        template: 'adoptionko',
                        message: {
                            to: update.adopteremail
                        },
                        locals: {
                            name: adoption.name
                        }
                    })
                    .then()
                    .catch(console.error);


            }


        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }





    },
    async deleteAdoption(req,res,next){

        let id = req.params.id;
        try{
            let adoption = await Adoption.findByIdAndRemove(id).populate("user adopter").exec();
            adoption = adoption.toObject();
            for(let index in adoption.media){
                const file = './uploads/adoptions/'+adoption.media[index];
                if(fs.existsSync(file)) {
                    fs.unlinkSync(file);
                }
            }
            res.json(adoption);
        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }
    },

    async uploadImage(req, res, next){

        const adoptionid = req.body.adoption;
        const cover = !!req.body.cover;

        if(req.files){

            const path = req.files.file.path;
            const split = path.split("/");
            let name = split[split.length-1];
            const extsplit = name.split('\.');
            const ext = extsplit[extsplit.length-1];
            extsplit.splice(-1,1);
            name = extsplit.join();
            split.splice(-1,1);
            const route = split.join('/');

            if(ext === 'png' || ext === 'jpg' || ext === 'jpeg' || ext === 'gif'){



                const nameEdit = route+"/"+name+'LOW.'+ext;
                sharp(path)
                    .resize(1920,1920).max()
                    .withoutEnlargement(true)
                    .jpeg({
                        quality: 20,
                        progresive: true,
                        chromaSubsampling: '4:4:4',
                        optimiseScans: true
                    })
                    .toFile(nameEdit).then(async function(){

                    fs.unlinkSync(path);
                    let adoption = await Adoption.findByIdAndUpdate(adoptionid, {$push: {media: name+'LOW.'+ext}}, {new: true}).exec();
                    if(cover){
                        adoption = await Adoption.findByIdAndUpdate(adoptionid, {cover: name+'LOW.'+ext}, {new: true}).exec();
                    }
                    res.json(adoption);


                });

            }else{
                fs.unlinkSync(path);
                let err = new Error('Tipo de fichero no soportado');
                err.status = 415;
                next(err);
            }

        }else{
            let err = new Error('No se ha subido ninguna imagen');
            err.status = 403;
            next(err);
        }

    },


    async getImage(req, res, next){

        const id = req.params.id;
        const pathfile = './uploads/adoptions/' + id;

        fs.exists(pathfile, function (exist) {
            if (exist) {
                res.sendFile(path.resolve(pathfile));
            } else {
                let err = new Error('No se encuentra la imagen');
                err.status = 404;
                next(err);
            }
        });
    },

    async deleteImage(req, res, next){

        const id = req.params.id;
        const pathfile = './uploads/adoptions/' + id;
        try {
            if (id) {
                if (fs.existsSync(pathfile)) {
                    fs.unlinkSync(pathfile);
                }
                const adoption = await Adoption.update({media: id}, {$pull: {media: id}});
                res.json(adoption);
            }else{
                let err = new Error('No se encuentra la imagen');
                err.status = 404;
                next(err);
            }
        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },
    async adopt(req,res,next){

        const id = req.params.id;
        const data = req.body;

        const adoption = await Adoption.findById(id).exec();

        if (adoption.adopted === 0) {

            let update = {};
            update.adopterData = data;
            update.adopted = 1;

            try {
                const adoption = await Adoption.findByIdAndUpdate(id, update, {
                    new: true,
                    runValidators: true
                }).populate("user adopter").exec();

                res.json(adoption);
            } catch (e) {
                const txterr = errorutils.mongooseErrors(e);
                let err = new Error(txterr.txt);
                err.status = txterr.code;
                next(err);
            }


        } else {
            let err = new Error('Lo sentimos, ' + adoption.name + ' ya tiene una solicitud de adopción.');
            err.status = 409;
            next(err);
        }

    }




};