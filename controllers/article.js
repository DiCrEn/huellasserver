'use strict'

const User = require('../models/user');
const Role = require('../models/role');
const Comment = require('../models/comment');
const errorutils = require("../utils/errorsutils");
const databaseutils = require("../utils/databaseutils");
const Article = require('../models/article');
const sanitizehtml = require('sanitize-html');
const mongoose = require('mongoose');

//https://github.com/punkave/sanitize-html
const sanitizecfg = {
    allowedTags: [ 'h1','h2','h3', 'h4', 'h5', 'h6', 'blockquote', 'p', 'a', 'ul', 'ol','u',
        'nl', 'li', 'b', 'i', 'strong', 'em', 'strike', 'code', 'hr', 'br', 'div',
        'table', 'thead', 'caption', 'tbody', 'tr', 'th', 'td', 'pre', 'iframe', 'img' ],
    allowedAttributes: {
        a: [ 'href', 'name', 'target' ],
        div: [ 'style' ],
        img: [ 'src' ]
    },
    allowedStyles: {
        '*': {
            // Match HEX and RGB
            'color': [/^\#(0x)?[0-9a-f]+$/i, /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/],
            'text-align': [/^left$/, /^right$/, /^center$/],
            // Match any number with px, em, or %
            'font-size': [/^\d+(?:px|em|%)$/]
        }
    },

    selfClosing: [ 'img', 'br', 'hr', 'area', 'base', 'basefont', 'input', 'link', 'meta' ],

    allowedSchemes: [ 'http', 'https', 'ftp', 'mailto', 'data' ],
    allowedSchemesByTag: {},
    allowedSchemesAppliedToAttributes: [ 'href', 'src', 'cite' ],
    allowProtocolRelative: true,
    allowedIframeHostnames: ['www.youtube.com', 'player.vimeo.com']
};

module.exports = {

    async newArticle(req,res,next){
        const article = new Article();

        article.title = req.body.title;
        article.text = sanitizehtml(req.body.text, sanitizecfg);
        article.tags = req.body.tags;
        article.user = req.user;

        try {
            const articleInserted = await article.save();
            res.json(articleInserted);
        }catch(error){
            const txterr = errorutils.mongooseErrors(error);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },
    async getArticle(req,res,next){

        //Las selecciones de la búsqueda o por defecto
        const search = req.query.search || "";
        const orderby = req.query.orderby || "date";
        const order = req.query.order || -1;
        const tag = req.query.tag || undefined;
        const count = req.query.count || 0;

        const limit = parseInt(req.query.perpage) || 10;
        const page = parseInt(req.query.page)-1 || 0;
        const skip = page * (limit);


        const find = {[tag ? "tags" : undefined]: tag,
            $or: [
                {text: {"$regex": search, "$options": "i"}},
                {title: {"$regex": search, "$options": "i"}},
                {tags: search }
            ]};

        const totalCount = await Article.count(find);

        if(count){
            res.json(totalCount);
            return;
        }

        let pipeline = [
            {
                $lookup: {
                    from: "comments",
                    localField: "_id",
                    foreignField: "article",
                    as: "comments"
                }
            },
            {
                $project: {
                    _id: true,
                    tags: true,
                    date: true,
                    title: true,
                    text: true,
                    user: true,
                    numcomments: {
                        $size: "$comments"
                    },
                }
            },
            {
                $sort: {
                    [orderby]: parseInt(order)
                }
            },
            {
                $match: find
            }
        ];

        try {

            if(req.params.id){
                pipeline.unshift({$match: {_id: mongoose.Types.ObjectId(req.params.id)}});
            }


            let article = await Article.aggregate(pipeline).skip(skip).limit(limit).exec();


            //Obtengo los datos de los respectivos usuarios de los articulos y sus roles
            await Role.populate(article,{path: 'user', model: 'User', populate: {path: 'role', model: 'Role'}});


            if(req.params.id) {
                if (article[0]) {
                    //Si es sólo un artículo obtengo también sus comentarios.
                    article = await databaseutils.reversePopulation(article,Comment,"article","comments",{
                        populate: [{ path: 'user', model: 'User',
                                        populate: {path: 'role', model: 'Role'}
                                    }, {
                                        path: 'vote.user', model: 'User'
                                    }]
                        });
                    res.json(article[0]);
                } else {
                    let err = new Error('No encontrado');
                    err.status = 404;
                    next(err);
                }
            }else {

                const result = {
                    "totalRecords": totalCount,
                    "page": page+1,
                    "result": article
                };

                res.json(result);
            }
        }catch(error){
            const txterr = errorutils.mongooseErrors(error);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }


    },
    async editArticle(req,res,next){

        let id = req.params.id;
        let update = req.body;
        update.text = sanitizehtml(update.text, sanitizecfg);

        try {
            const article = await Article.findByIdAndUpdate(id, update, {new: true, runValidators: true}).populate(
                {path: 'user', model: 'User', populate:
                        {
                            path: 'role',
                            model: 'Role'
                        }
                }).exec();
            res.json(article);
        }catch(e){
            const txterr = errorutils.mongooseErrors(e);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }

    },
    async deleteArticle(req,res,next){

        let id = req.params.id;
        try{
            //No uso findByIdAndRemove porque así no se llama al middleware .pre, con .remove() si.
            const article = await Article.findById(id).populate({path: 'user', model: 'User', populate: {path: 'role', model: 'Role'}}).exec();
            await article.remove();

            res.json(article);
        }catch(error){
            const txterr = errorutils.mongooseErrors(error);
            let err = new Error(txterr.txt);
            err.status = txterr.code;
            next(err);
        }
    },

    async getTags(req,res,next){

        const articlestags = await Article.find({}).select('tags -_id');

        let all = [];
        for(let indextags in articlestags){
            // noinspection JSUnfilteredForInLoop
            all = all.concat(articlestags[indextags].tags);
        }
        //Borrar repetidos
        all = all.filter(function(item, pos) {
            return all.indexOf(item) === pos;
        });
        res.json(all);
    },

    async getSearchTag(req,res,next){

        const tag = req.params.tag;
        const articles = await Article.find({tags: tag }).populate(
            {path: 'user', model: 'User', populate:
                    {
                        path: 'role',
                        model: 'Role'
                    }
            });
        res.json(articles);

    },



};