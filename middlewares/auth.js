const jwt = require('jwt-simple');
const config = require('config');
const moment = require('moment');
const User = require('../models/user');

const auth = {

    ensureAuthenticated(req, res, next) {
        if(!req.headers.authorization) {
            return res
                .status(401)
                .send({message: "Tu petición no tiene cabecera de autorización"});
        }

        let payload = jwt.decode(req.headers.authorization, config.get("TOKEN_SECRET"));

        if(payload.exp <= moment().unix()) {
            return res
                .status(401)
                .send({message: "El token ha expirado"});
        }

        User.findById(payload.sub).populate({path: 'role'}).exec(function(error,user){

            if(error){
                const errort = errorutils.mongooseErrors(error);
                let err = new Error(errort.txt);
                err.status = 401;
                next(err);
            }else if(!user){
                let err = new Error('El usuario no existe');
                err.status = 401;
                next(err);
            }else{
                req.user = user._id;
                if(user.role) {
                    req.permissions = user.role.permissions;
                }
                next();
            }
        });
    },


    maybeAuthenticated(req, res, next) {

        if(!req.headers.authorization) {
            next();
        }else {
            let payload = jwt.decode(req.headers.authorization, config.get("TOKEN_SECRET"));

            if (payload.exp <= moment().unix()) {
                next();
            }else {
                User.findById(payload.sub).populate({path: 'role'}).exec(function (error, user) {
                    if (error || !user) {
                        next();
                    } else {
                        req.user = user._id;
                        if (user.role) {
                            req.permissions = user.role.permissions;
                        }
                        next();
                    }
                });
            }
        }
    },

};

module.exports = auth;