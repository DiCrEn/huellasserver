const roleutils = require("../utils/roleutils");
const Article = require('../models/article');
const Comment = require('../models/comment');
const Adoption = require('../models/adoption');

const permissions = {


    checkPermissions(role) {
        return function(req, res, next) {

            if(roleutils.isPermitted(req.permissions,role)){
                next();
            }else{
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                next(err);
            }
        }
    },
    //Tergo permisos para editar usuarios o estoy editando mi propio usuario
    userWritePermitted(req, res, next){
        let id = req.params.id;
        //NO estoy editando mi usuario, a si que compruebo permisos
        if(id.toString() !== req.user.toString()){
            if(!roleutils.isPermitted(req.permissions, roleutils.ROLE_LIST.user.e)){
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                next(err);
            }else{
                next();
            }
        }else{
            next();
        }
    },

    //Tergo permisos para editar articulos o estoy editando mi propio articulo
    async articleWritePermitted(req, res, next){
        let id = req.params.id;
        const article = await Article.findById(id);
        //NO estoy editando mi usuario, a si que compruebo permisos
        if(article.user.toString() !== req.user.toString()){
            if(!roleutils.isPermitted(req.permissions, roleutils.ROLE_LIST.article.e)){
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                next(err);
            }else{
                next();
            }
        }else{
            next();
        }
    },

    //Tergo permisos para editar articulos o estoy editando mi propio articulo
    async commentWritePermitted(req, res, next){
        let id = req.params.id;
        const comment = await Comment.findById(id);
        //NO estoy editando mi usuario, a si que compruebo permisos
        if(comment.user.toString() !== req.user.toString()){
            if(!roleutils.isPermitted(req.permissions, roleutils.ROLE_LIST.comment.e)){
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                next(err);
            }else{
                next();
            }
        }else{
            next();
        }
    },

    //Tergo permisos para editar adopciones o estoy editando mi propia adopcion
    async adoptionWritePermitted(req, res, next){
        let id = req.params.id;
        const adoption = await Adoption.findById(id);
        //NO estoy editando mi usuario, a si que compruebo permisos
        if(adoption.user.toString() !== req.user.toString()){
            if(!roleutils.isPermitted(req.permissions, roleutils.ROLE_LIST.adoption.e)){
                let err = new Error('No estás autorizado para realizar esa acción');
                err.status = 403;
                next(err);
            }else{
                next();
            }
        }else{
            next();
        }
    }


};


module.exports = permissions;