
const validationUtils = require('../utils/validationutils');
const User = require('../models/user');
const Article = require('../models/article');
const Comment = require('../models/comment');
const Adoption = require('../models/adoption');

const validations = {

    validemail(req,res,next){
        let email = req.body.email;
        if(validationUtils.emailvalidator(email)){
            next();
        }else{
            let err = new Error("No es un email válido");
            err.status = 422;
            next(err);
        }
    },
    emailexist(req, res, next){
        let email = req.body.email;

        User.findOne({email: email.toLowerCase()}, (err,user) => {
            if(err){
                console.log(err);
                let err = new Error('Error al comprobar el email');
                err.status = 500;
                next(err);
            }else if(user){
                if(req.params.id !== undefined && req.params.id.toString() === user._id.toString()){ //Si, existe, pero estoy editandolo.
                    next();
                }else {
                    let err = new Error('El email ya está registrado');
                    err.status = 409;
                    next(err);
                }
            }else{
                next();
            }
        });
    },

    async articleexist(req,res,next){
        let id = req.params.id;
        try {
            const article = await Article.findById(id).exec();
            if(article) next();
            else {
                let err = new Error('No encontrado');
                err.status = 404;
                next(err);
            }
        }catch(e){
            let err = new Error('No encontrado');
            err.status = 404;
            next(err);
        }
    },

    async commentexist(req,res,next){
        let id = req.params.id;
        try {
            const comment = await Comment.findById(id).exec();
            if(comment) next();
            else {
                let err = new Error('No encontrado');
                err.status = 404;
                next(err);
            }
        }catch(e){
            let err = new Error('No encontrado');
            err.status = 404;
            next(err);
        }
    },

    async adoptionexist(req,res,next){
        let id = req.params.id;
        try {
            const adoption = await Adoption.findById(id).exec();
            if(adoption) next();
            else {
                let err = new Error('No encontrado');
                err.status = 404;
                next(err);
            }
        }catch(e){
            let err = new Error('No encontrado');
            err.status = 404;
            next(err);
        }
    },



};

module.exports = validations;