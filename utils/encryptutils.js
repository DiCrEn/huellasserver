'use strict'

const crypto = require('crypto');
const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('config');

module.exports = {

    encryptpass(password) {
        let hash = crypto.createHmac('sha512', config.get("SALT"));
        hash.update(password);
        return hash.digest('hex');
    },


    createToken(iduser) {
        let payload = {
            sub: iduser,
            iat: moment().unix(),
            exp: moment().add(14, "days").unix(),
        };
        return jwt.encode(payload, config.get("TOKEN_SECRET"));
    }




};