const langutils = require('./langutils');
const config = require('config');

module.exports = {

    mongooseErrors(errors){

        let txt = "";
        let errorCode = 500;

        if(errors.message && errors.message === "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters"){
            //Está pidiendo un id y le hemos dado cualquier cosa, a si que 404
            txt+="No encontrado";
            errorCode = 404;
        }else if(errors.code === 11000 || errors.code ===  2){
            //Envio el error tal cual. Debería haber un middleware que evite que salga este error al usuario.
            txt+=errors.errmsg;
            errorCode=errors.code;

        }else if(errors.name==="CastError" && errors.path==="_id"){
            //No se puede convertir el id, por lo tanto no existe ese elemento, a si que 404
            txt+="No encontrado";
            errorCode = 404;
        }else{

            try {
                const keys = Object.keys(errors.errors);

                for (let index in keys) {
                    const error = errors.errors[keys[index]];

                    //Campo requerido
                    if (error.kind === 'required') {
                        txt += `El campo ${langutils.translate(error.path)} es obligatorio.\n`;
                        errorCode=400;
                    } else {
                        console.log("ERROR NO TRATADO");
                        console.log(error);
                        txt += "Error desconocido";
                    }


                }
            }catch(e){
                //Si estoy en desarrollo imprimo el error de mongo, si no envio uno generico
                if(config.util.getEnv('NODE_ENV') === 'dev') txt+=errors.toString();
                else txt+="Error del sistema";
            }
        }

        return {txt: txt,
                code: errorCode};
    }


};