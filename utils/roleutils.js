const roleutils = {
    ROLE_LIST: {
        article: {
            w: parseInt("1",2), //Crear post y editar el mio
            e: parseInt("10",2), //Editar el de los demás
        },
        adoption: {
            w: parseInt("100",2), //Crear ficha y editar la mia
            e: parseInt("1000",2), //Editar el de los demás
        },
        comment: {
            w: parseInt("10000",2), //Crear comentario y editar el mio
            e: parseInt("100000",2), //Editar el de los demás
        },
        user: {
            r: parseInt("1000000",2), //Leer datos sensibles de los usuarios
            e: parseInt("10000000",2), //Se asume que puede editar el de los demás.
        },
        role: {
            w: parseInt("100000000",2), //Puede crear editar y eliminar roles.
        },
    },

    //Sumo todos los roles.
    getRoleAdmin() {
        let total = 0;
        for (let index in roleutils.ROLE_LIST) {
            for (let index2 in roleutils.ROLE_LIST[index]) {
                total += roleutils.ROLE_LIST[index][index2];
            }
        }
        return total;
    },
    isPermitted(userPermissions,permission){
        if(permission===0) return true; //No estoy pidiendo ningún permiso
        else if(userPermissions === undefined || userPermissions === 0) return false; //Sin permisos
        else{
            // noinspection JSBitwiseOperatorUsage
            return !((userPermissions & permission) === 0);
        }
    }



};

module.exports = roleutils;