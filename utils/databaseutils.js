'use strict'

const _ = require('lodash');
const mongoose = require('mongoose');


module.exports = {

    /**
     *
     * @param data Array to populate
     * @param Type Model
     * @param field field reference
     * @param ref new property for data
     * @param options optional. options.populate, options.sort, options.limit, options.select
     * @param method optional default find
     * @returns {Promise<Array>}
     */
    async reversePopulation(data, Type, field, ref, options, method="find"){

        if(!options) options = {};

        const that = this;

        const funcReversePop = async function(data){

            let dataobj;
            if(that.isMongooseObject(data)) dataobj = data.toObject();
            else dataobj = data;
            const find = {};
            find[field] = dataobj._id;
            const petition = Type[method](find);
            if (options.populate) petition.populate(options.populate);
            if (options.sort) petition.sort(options.sort);
            if(options.limit) petition.limit(options.limit);
            if (options.select) petition.select(options.select);
            let newData = await petition.exec();
            if(options.functionPerData){
                newData = await options.functionPerData(newData);
            }
            dataobj[ref] = newData;
            return dataobj;

        };

        if(Array.isArray(data)) {
            let newArray = [];
            for (let i = 0; i < data.length; i++) {
                try {
                    newArray[i] = await funcReversePop(data[i]);
                }catch(e){
                    newArray[i] = e;
                }
            }
            return newArray;
        }else{
            try {
                return await funcReversePop(data);
            }catch(e){
                return e;
            }
        }

    },

    isMongooseObject(obj) {
        return _.get(obj, 'constructor.base') instanceof mongoose.Mongoose;
    }
    
};