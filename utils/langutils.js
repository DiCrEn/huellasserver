/*
 * Se utiliza para traducir los campos de mongodb al castellano en los errores,
 * que si no queda muy raro en spaniglish.
 */

const translations = {
    "name":"nombre",
    "lastname":"apellido",
    "text":"texto",
    "animal":"animal",
    "sex":"género",
    "cover":"portada",
    "date":"fecha",
    "user":"usuario",
    "title":"titulo",
    "body":"cuerpo",
    "article":"artículo"

};


module.exports = {

    translate(word){
        let tr = translations[word];
        if(tr) return tr;
        else return word;
    }

};