'use strict'

process.env.NODE_ENV = 'test';
process.env.PORT = 7585;

global.mongoose = require("mongoose");
global.User = require('../models/user');
global.Role = require('../models/role');
global.config = require('config');

global.chai = require('chai');
global.chaiHttp = require('chai-http');
global.myserver = require('../index');
global.should = chai.should();
global.expect = chai.expect;

chai.use(chaiHttp);

describe('REST', () => {


    //delete DDBB
    after((done) => {

        let con = mongoose.connect(config.get("DBHOST"));
        mongoose.connection.once('open', function(){
            mongoose.connection.db.dropDatabase(function(err, result){
                done();
            });
        });
    });

    before((done) => {

        //Cuando el servidor alla arrancado registro el usuario de prueba y obtengo los datos necesarios.
        myserver.on( "app_started", async function() {

            //Registro usuario "dummy"
            let res = await chai.request(myserver).post('/api/v1/user/')
                .send({
                    name:"dummy",
                    lastname:"dummy",
                    email: "dummy@dummy.com",
                    password: "1234"
                });

            global.dummy = {
                token: res.body.token,
                id:   res.body._id,
                role: res.body.role._id
            };

            //Obtengo los datos del admin
            res = await chai.request(myserver).post('/api/v1/user/login')
                .send({
                    email: config.get("ADMIN_EMAIL"),
                    password: config.get("ADMIN_PASSWORD")
                });

            global.admin = {
                token: res.body.token,
                id:   res.body._id,
                role: res.body.role
            };

            done(); //Termino el before()

        });
    });

    it('check test data in ddbb', (done) => {

        dummy.should.be.a('object');
        admin.should.be.a('object');

        dummy.should.have.property('token');
        dummy.should.have.property('id');
        dummy.should.have.property('role');

        admin.should.have.property('token');
        admin.should.have.property('id');
        admin.should.have.property('role');

        done();
    });

    require('./REST/user.test');
    require('./REST/role.test');
    require('./REST/article.test');
    require('./REST/comment.test');
    require('./REST/adoption.test');


});