'use strict'

const {ROLE_LIST} = require("../../utils/roleutils");

let articledummy="";
let articleadmin="";

describe('Articles', () => {

    before(async () => {

        //Add permissions to dummy's role
        let res = await chai.request(myserver).put('/api/v1/role/' + dummy.role)
            .set("Authorization", admin.token)
            .send({
                permissions: ROLE_LIST.article.w
            });

        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('permissions').eql(ROLE_LIST.article.w);
        res.body.should.have.property('_id');

    });

    describe('/POST Add new article', () => {

        it('New article without token', async () => {
            let res = await chai.request(myserver).post('/api/v1/article/')
                .send({
                    title:"titulo",
                    text:"texto",
                    tags:[
                        "tag1","tag2"
                    ]
                });

            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("Tu petición no tiene cabecera de autorización");
        });

        it('New article from dummy', async () => {
            let res = await chai.request(myserver).post('/api/v1/article/')
                .set("Authorization",dummy.token)
                .send({
                    title:"titulo",
                    text:"texto",
                    tags:[
                        "tag1","tag2"
                    ]
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user').eql(dummy.id);
            res.body.should.have.property('title').eql("titulo");
            res.body.should.have.property('tags').be.a('array');
            articledummy = res.body._id;
        });

        it('New article from admin', async () => {
            let res = await chai.request(myserver).post('/api/v1/article/')
                .set("Authorization",admin.token)
                .send({
                    title:"titulo2",
                    text:"texto2",
                    tags:[
                        "tag3","tag4"
                    ]
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user').eql(admin.id);
            res.body.should.have.property('title').eql("titulo2");
            res.body.should.have.property('tags').be.a('array');
            articleadmin = res.body._id;
        });

        it('Test sanitize-html', async () => {
            let res = await chai.request(myserver).post('/api/v1/article/')
                .set("Authorization",admin.token)
                .send({
                    title:"Código malicioso",
                    text:`¡Romper internet!<script>searchInGoogle('google');</script>`,
                    tags:[
                        "tag3","tag4"
                    ]
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user').eql(admin.id);
            res.body.should.have.property('title').eql("Código malicioso");
            res.body.should.have.property('text').eql("¡Romper internet!");
            res.body.should.have.property('tags').be.a('array');
            articleadmin = res.body._id;
        });



    });

    describe('/GET article', () => {

        it('Get article list', async () => {
            let res = await chai.request(myserver).get('/api/v1/article/');

            res.should.have.status(200);
            res.body.result.should.to.have.length(3);
            res.body.result.should.be.a('array');
            res.body.result.every(i => {
                expect(i).to.have.all.keys(
                    '_id',
                    'numcomments',
                    'date',
                    'tags',
                    'title',
                    'text',
                    'user');
            });
            res.body.result.every(i => {
                i.numcomments.should.to.be.a('number');
            });
        });

        it('Get article', async () => {
            let res = await chai.request(myserver).get('/api/v1/article/'+articleadmin);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.to.have.all.keys(
                    '_id',
                    'date',
                    'tags',
                    'title',
                    'text',
                    'numcomments',
                    'comments',
                    'user');
            res.body.comments.should.be.a('array');

        });


        it('Get article not exist', async () => {
            let res = await chai.request(myserver).get('/api/v1/article/'+'333');
            res.should.have.status(404);
        });

        it('Get list of tags', async () => {
            let res = await chai.request(myserver).get('/api/v1/article/tags/');
            res.should.have.status(200);
            res.body.should.to.have.length(4);
        });

        it('Search by tag', async () => {
            let res = await chai.request(myserver).get('/api/v1/article/?tag='+"tag3");
            res.should.have.status(200);
            res.body.result.should.to.have.length(2);
        });

        it('Search tag by text', async () => {
            let res = await chai.request(myserver).get('/api/v1/article?search='+"tag3");
            res.should.have.status(200);
            res.body.result.should.to.have.length(2);
        });

        it('Search by text', async () => {
            let res = await chai.request(myserver).get('/api/v1/article?search='+"malicioso");
            res.should.have.status(200);
            res.body.result.should.to.have.length(1);
        });

        it('Search by text uppercase and incomplete word', async () => {
            let res = await chai.request(myserver).get('/api/v1/article?search='+"MALICIOS");
            res.should.have.status(200);
            res.body.result.should.to.have.length(1);
        });

    });

    describe('/PUT article', () => {

        it('Admin edit dummy\'s article', async () => {
            let res = await chai.request(myserver).put('/api/v1/article/'+articledummy)
                .set("Authorization",admin.token)
                .send({
                    title:"titulo EDIT BY ADMIN",
                    text:"texto EDIT BY ADMIN",
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user');
            res.body.user._id.should.eql(dummy.id);
            res.body.should.have.property('title').eql("titulo EDIT BY ADMIN");
            res.body.should.have.property('tags').be.a('array');

        });

        it("Dummy edit admin's article", async () => {
            let res = await chai.request(myserver).put('/api/v1/article/'+articleadmin)
                .set("Authorization",dummy.token)
                .send({
                    title:"titulo EDIT BY DUMMY",
                    text:"texto EDIT BY DUMMY",
                });
            res.should.have.status(403);
        });

        it('Dummy edit dummy\'s article', async () => {
            let res = await chai.request(myserver).put('/api/v1/article/'+articledummy)
                .set("Authorization",dummy.token)
                .send({
                    title:"titulo EDIT BY DUMMY",
                    text:"texto EDIT BY DUMMY",
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user');
            res.body.user._id.should.eql(dummy.id);
            res.body.should.have.property('title').eql("titulo EDIT BY DUMMY");
            res.body.should.have.property('tags').be.a('array');

        });


    });

    describe('/DELETE article', () => {

        it('Dummy delete admin\'s article', async () => {
            let res = await chai.request(myserver).delete('/api/v1/article/'+articleadmin)
                .set("Authorization",dummy.token).send();
            res.should.have.status(403);
        });

        it('Dummy delete dummy\'s article', async () => {
            let res = await chai.request(myserver).delete('/api/v1/article/'+articledummy)
                .set("Authorization",dummy.token).send();
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user');
            res.body.user._id.should.eql(dummy.id);
            res.body.should.have.property('tags').be.a('array');
        });

        it('Admin delete admin\'s article', async () => {
            let res = await chai.request(myserver).delete('/api/v1/article/'+articleadmin)
                .set("Authorization",admin.token).send();
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user');
            res.body.user._id.should.eql(admin.id);
            res.body.should.have.property('tags').be.a('array');
        });

        it('Admin delete admin\'s article again', async () => {
            let res = await chai.request(myserver).delete('/api/v1/article/'+articleadmin)
                .set("Authorization",admin.token).send();
            res.should.have.status(404);
        });


    });

});