'use strict'

const {ROLE_LIST} = require("../../utils/roleutils");

let articleid="";
let dummycomment="";
let adminomment="";


describe('Comments', () => {

    before(async () => {

        //Add permissions to dummy's role
        let res = await chai.request(myserver).put('/api/v1/role/' + dummy.role)
            .set("Authorization", admin.token)
            .send({
                permissions: ROLE_LIST.comment.w
            });



        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('permissions').eql(ROLE_LIST.comment.w);
        res.body.should.have.property('_id');

        res = await chai.request(myserver).get('/api/v1/article/');


        articleid = res.body.result[0]._id;

    });

    describe('/POST Add new comment', () => {

        it('New comment without token', async () => {
            let res = await chai.request(myserver).post('/api/v1/comment/')
                .send({
                    article:articleid,
                    text:"texto"
                });

            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("Tu petición no tiene cabecera de autorización");
        });

        it('New comment by dummy', async () => {
            let res = await chai.request(myserver).post('/api/v1/comment/')
                .set("Authorization",dummy.token)
                .send({
                    article:articleid,
                    text:"texto"
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('text').eql("texto");
            dummycomment=res.body._id;
        });

        it('New comment by admin', async () => {
            let res = await chai.request(myserver).post('/api/v1/comment/')
                .set("Authorization",admin.token)
                .send({
                    article:articleid,
                    text:"textoadmin"
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('text').eql("textoadmin");
            adminomment=res.body._id;
        });

    });

    describe('/GET Article with comment', () => {

        it('Get article', async () => {
            let res = await chai.request(myserver).get('/api/v1/article/'+articleid);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.comments.should.be.a('array');
            res.body.comments.should.to.have.length(2);

        });

    });

    describe('/PUT comment', () => {

        it('Admin edit dummy\'s comment', async () => {
            let res = await chai.request(myserver).put('/api/v1/comment/'+dummycomment)
                .set("Authorization",admin.token)
                .send({
                    text:"texto EDIT BY ADMIN"
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('text').eql("texto EDIT BY ADMIN");
            res.body.user._id.should.eql(dummy.id);

        });

        it('Dummy edit admin\'s comment', async () => {
            let res = await chai.request(myserver).put('/api/v1/comment/'+adminomment)
                .set("Authorization",dummy.token)
                .send({
                    text:"texto EDIT BY DUMMY"
                });

            res.should.have.status(403);

        });


        it('Dummy edit dummy\'s comment', async () => {
            let res = await chai.request(myserver).put('/api/v1/comment/'+dummycomment)
                .set("Authorization",dummy.token)
                .send({
                    text:"texto EDIT BY DUMMY"
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('text').eql("texto EDIT BY DUMMY");
            res.body.user._id.should.eql(dummy.id);

        });

    });

    describe('/DELETE comment', () => {

        it('Dummy delete admin\'s comment', async () => {
            let res = await chai.request(myserver).delete('/api/v1/comment/'+adminomment)
                .set("Authorization",dummy.token).send();
            res.should.have.status(403);
        });

        it('Dummy delete dummy\'s comment', async () => {
            let res = await chai.request(myserver).delete('/api/v1/comment/'+dummycomment)
                .set("Authorization",dummy.token).send();
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.user.should.eql(dummy.id);
        });

        it('Admin delete admin\'s comment', async () => {
            let res = await chai.request(myserver).delete('/api/v1/comment/'+adminomment)
                .set("Authorization",admin.token).send();
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.user.should.eql(admin.id);
        });

        it('Admin delete admin\'s comment again', async () => {
            let res = await chai.request(myserver).delete('/api/v1/comment/'+adminomment)
                .set("Authorization",admin.token).send();
            res.should.have.status(404);
        });


    });



});