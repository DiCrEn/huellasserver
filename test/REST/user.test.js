'use strict'

let tokenAdmin = "";
let tokenUser1 = "";
let tokenUser2 = "";

let idUser1 = "";
let idUser2 = "";
let idAdmin = "";

let adminrole = "";
let userrole = "";

let userData1 = {
    name:"Nuria",
    lastname:"Apellidos",
    email: "nuria@prueba.com",
    password: "1234"
};
let userData2 = {
    name:"Carmen",
    lastname:"García",
    email: "carmen@prueba.com",
    password: "1234"
};

describe('Users', () => {

    describe('/POST Register', () => {
        it('Register wrong email', (done) => {
            let data = {
                email: "wrongemail",
                password: "1234"
            };
            chai.request(myserver)
                .post('/api/v1/user/')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql("No es un email válido");
                    res.body.should.not.have.property('lastname');
                    res.body.should.not.have.property('token');
                    done();
                });
        });
        it('Register without name and lastname', (done) => {
            let data = {
                email: "prueba@prueba.com",
                password: "1234"
            };
            chai.request(myserver)
                .post('/api/v1/user/')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').contains("El campo nombre es obligatorio");
                    res.body.should.not.have.property('lastname');
                    res.body.should.not.have.property('token');
                    done();
                });
        });
       it('Register success', (done) => {
            chai.request(myserver)
                .post('/api/v1/user/')
                .send(userData1)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').eql(userData1.name);
                    res.body.should.have.property('lastname').eql(userData1.lastname);
                    res.body.should.have.property('token');
                    res.body.should.have.property('email').eql(userData1.email);
                    res.body.should.not.have.property('password');
                    tokenUser1 = res.body.token;
                    idUser1 = res.body._id;
                    userrole = res.body.role;
                    done();
                });
        });
        it('Register repeated', (done) => {
            chai.request(myserver)
                .post('/api/v1/user/')
                .send(userData1)
                .end((err, res) => {
                    res.should.have.status(409);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').contains("El email ya está registrado");
                    res.body.should.not.have.property('lastname');
                    res.body.should.not.have.property('token');
                    done();
                });
        });
        it('Register 2 success', (done) => {
            chai.request(myserver)
                .post('/api/v1/user/')
                .send(userData2)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').eql(userData2.name);
                    res.body.should.have.property('lastname').eql(userData2.lastname);
                    res.body.should.have.property('token');
                    res.body.should.have.property('email').eql(userData2.email);
                    res.body.should.not.have.property('password');
                    tokenUser1 = res.body.token;
                    done();
                });
        });
    });

    /*
    * Test the Login
    */
    describe('/POST login', () => {
        it('Admin login', (done) => {
            let adminData = {
                email: config.get("ADMIN_EMAIL"),
                password: config.get("ADMIN_PASSWORD")
            };
            chai.request(myserver)
                .post('/api/v1/user/login')
                .send(adminData)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name');
                    res.body.should.have.property('lastname');
                    res.body.should.have.property('token');
                    res.body.should.have.property('role');
                    res.body.should.have.property('email').eql(config.get("ADMIN_EMAIL"));
                    res.body.should.not.have.property('password');
                    tokenAdmin = res.body.token;
                    adminrole = res.body.role;
                    idAdmin = res.body._id;
                    done();
                });
        });
        it('user login', (done) => {
            let data = {
                email: userData1.email,
                password: userData1.password
            };
            chai.request(myserver)
                .post('/api/v1/user/login')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name');
                    res.body.should.have.property('lastname');
                    res.body.should.have.property('token');
                    res.body.should.have.property('email').eql(userData1.email);
                    res.body.should.not.have.property('password');
                    tokenUser1 = res.body.token;
                    idUser1 = res.body._id;
                    done();
                });
        });
        it('user 2 login', (done) => {
            let data = {
                email: userData2.email,
                password: userData2.password
            };
            chai.request(myserver)
                .post('/api/v1/user/login')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name');
                    res.body.should.have.property('lastname');
                    res.body.should.have.property('token');
                    res.body.should.have.property('email').eql(userData2.email);
                    res.body.should.not.have.property('password');
                    tokenUser2 = res.body.token;
                    idUser2 = res.body._id;
                    done();
                });
        });
        it('wrong login', (done) => {
            let data = {
                email: userData2.email,
                password: userData2.password+"aaa"
            };
            chai.request(myserver)
                .post('/api/v1/user/login')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(403);
                    res.body.should.be.a('object');
                    res.body.should.not.have.property('name');
                    res.body.should.not.have.property('token');
                    res.body.should.not.have.property('password');
                    res.body.should.have.property('message').contains("Email o contraseña inválidos");

                    done();
                });
        });

    });

    describe('/GET users', () => {
        it('List users with admin', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/')
                .set("Authorization",tokenAdmin)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.result.should.be.a('array');
                    res.body.result.should.to.have.length(4);
                    res.body.result.every(i => {
                        expect(i).to.have.all.keys(
                        '_id',
                        'name',
                        'role',
                        'email', //With email
                        'date',
                        'lastActivity',
                        'fullname',
                        'numadoptions',
                        'numarticles',
                        'numcomments',
                        'lastname');
                        expect(i).to.have.nested.property('role.name');
                    });

                    done();
                });
        });
        it('List users with user', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/')
                .set("Authorization",tokenUser1)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.result.should.be.a('array');
                    res.body.result.should.to.have.length(4);
                    res.body.result.every(i => {
                        expect(i).to.have.all.keys(
                        '_id',
                        'name',
                        'role', //without email
                        'date',
                        'email',
                        'fullname',
                        'lastActivity',
                        'numadoptions',
                        'numarticles',
                        'numcomments',
                        'lastname');
                        expect(i).to.have.nested.property('role.name');
                        expect(i).to.have.property('email').eql(null);
                    });

                    done();
                });
        });
        it('get 1 user', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/'+idUser2)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.name.should.eql(userData2.name);
                    res.body.should.not.have.property('password');
                    res.body.should.have.property('email').eql(null);
                    done();
                });
        });
        it('get own profile', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/'+idUser2)
                .set("Authorization",tokenUser2)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.name.should.eql(userData2.name);
                    res.body.should.not.have.property('password');
                    res.body.should.have.property('email');
                    done();
                });
        });
        it('get other profile', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/'+idUser2)
                .set("Authorization",tokenUser1)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.name.should.eql(userData2.name);
                    res.body.should.not.have.property('password');
                    res.body.should.have.property('email').eql(null);
                    done();
                });
        });
        it('get 1 user with admin', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/'+idUser2)
                .set("Authorization",tokenAdmin)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.name.should.eql(userData2.name);
                    res.body.should.not.have.property('password');
                    res.body.should.have.property('email');
                    done();
                });
        });
    });


    describe('/PUT users', () => {
        it('Promote user himself', (done) => {
            chai.request(myserver)
                .put('/api/v1/user/'+idUser2)
                .set("Authorization",tokenUser2)
                .send({
                    email: userData2.email,
                    role: adminrole
                })
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });
        it('Promote by admin', (done) => {
            chai.request(myserver)
                .put('/api/v1/user/'+idUser2)
                .set("Authorization",tokenAdmin)
                .send({
                    email: userData2.email,
                    role: adminrole
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
        it('degradate me (admin to user)', (done) => {
            chai.request(myserver)
                .put('/api/v1/user/'+idUser2)
                .set("Authorization",tokenUser2)
                .send({
                    email: userData2.email,
                    role: userrole
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('name');
                    res.body.should.have.property('lastname');
                    res.body.should.have.property('token');
                    done();
                });
        });
        it('Edit my profile', (done) => {
            chai.request(myserver)
                .put('/api/v1/user/'+idUser2)
                .set("Authorization",tokenUser2)
                .send({
                    email: userData2.email+"b",
                    name: userData2.name+"b",
                    lastname: userData2.lastname+"b"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('name');
                    res.body.should.have.property('lastname').eql(userData2.lastname+"b");
                    res.body.should.have.property('token');
                    done();
                });
        });
    });

    describe('/DELETE users', () => {
        it('Remove admin by user', (done) => {
            chai.request(myserver)
                .delete('/api/v1/user/'+idAdmin)
                .set("Authorization",tokenUser2)
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });
        it('Remove user by admin', (done) => {
            chai.request(myserver)
                .delete('/api/v1/user/'+idUser2)
                .set("Authorization",tokenAdmin)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('name').eql(userData2.name+"b");
                    done();
                });
        });
        it('Remove myself', (done) => {
            chai.request(myserver)
                .delete('/api/v1/user/'+idUser1)
                .set("Authorization",tokenUser1)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('name').eql(userData1.name);
                    done();
                });
        });
        it('List users with admin', (done) => {
            chai.request(myserver)
                .get('/api/v1/user/')
                .set("Authorization",tokenAdmin)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.result.should.be.a('array');
                    res.body.result.should.to.have.length(2);
                    res.body.result.every(i => {
                        expect(i).to.have.all.keys(
                            '_id',
                            'name',
                            'role',
                            'email', //With email
                            'date',
                            'fullname',
                            'lastActivity',
                            'numadoptions',
                            'numarticles',
                            'numcomments',
                            'lastname');
                        expect(i).to.have.nested.property('role.name');
                    });

                    done();
                });
        });
    });
});