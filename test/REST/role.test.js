'use strict'

let testrole="";

describe('Roles', () => {
    describe('/POST Add new role', () => {

        it('Register without token', async () => {
            let res = await chai.request(myserver).post('/api/v1/role/')
                .send({
                    name:"nuevorol",
                    permissions:"4",
                    icon:"user-tie"
                });

            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("Tu petición no tiene cabecera de autorización");
        });

        it('Register without permissions', async () => {
            let res = await chai.request(myserver).post('/api/v1/role/')
                .set("Authorization",dummy.token)
                .send({
                    name:"nuevorol",
                    permissions:"4",
                    icon:"user-tie"
                });

            res.should.have.status(403);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("No estás autorizado para realizar esa acción");
        });


        it('Register with permissions', async () => {
            let res = await chai.request(myserver).post('/api/v1/role/')
                .set("Authorization",admin.token)
                .send({
                    name:"nuevorol",
                    permissions:"4",
                    icon:"user-tie"
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name').eql("nuevorol");
            res.body.should.have.property('permissions').eql(4);
            res.body.should.have.property('icon').eql("user-tie");
            res.body.should.have.property('_id');
            testrole=res.body._id;
        });

        it('Same name again', async () => {
            let res = await chai.request(myserver).post('/api/v1/role/')
                .set("Authorization",admin.token)
                .send({
                    name:"nuevorol",
                    permissions:"4",
                    icon:"user-tie"
                });

            res.should.have.status(409);
            res.body.should.be.a('object');
            res.body.should.not.have.property('name');
            res.body.should.not.have.property('permissions');
            res.body.should.not.have.property('icon');
            res.body.should.not.have.property('_id');
        });


    });

    describe('/GET Roles', () => {

        it('Get role list', async () => {
            let res = await chai.request(myserver).get('/api/v1/role/');

            res.should.have.status(200);
            res.body.should.be.a('array');
            res.body.every(i => {
                expect(i).to.have.all.keys(
                    '__v',
                    '_id',
                    'name',
                    'icon',
                    'permissions',
                    'special',
                    'users');
            });
        });

        it('Get admin role', async () => {
            let res = await chai.request(myserver).get('/api/v1/role/'+admin.role._id);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.to.have.all.keys(
                    '__v',
                    '_id',
                    'name',
                    'icon',
                    'permissions',
                    'special',
                    'users');
        });

        it('Get inexist role', async () => {
            let res = await chai.request(myserver).get('/api/v1/role/noexisto');
            res.should.have.status(404);
            res.body.should.not.have.property('name');
            res.body.should.not.have.property('permissions');
            res.body.should.not.have.property('icon');
            res.body.should.not.have.property('_id');
        });

    });

    describe('/PUT Roles', () => {

        it('Edit without token', async () => {
            let res = await chai.request(myserver).put('/api/v1/role/'+dummy.role)
                .send({
                    name:"nuevorolEDIT",
                    permissions:"5",
                    icon: "fas fa-user"
                });

            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("Tu petición no tiene cabecera de autorización");
        });

        it('Edit with token', async () => {
            let res = await chai.request(myserver).put('/api/v1/role/'+testrole)
                .set("Authorization",admin.token)
                .send({
                    name:"nuevorolEDIT",
                    permissions:"5",
                    icon: "fas fa-user"
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name').eql("nuevorolEDIT");
            res.body.should.have.property('permissions').eql(5);
            res.body.should.have.property('icon').eql("fas fa-user");
            res.body.should.have.property('_id');

        });

        it('Get edited role', async () => {
            let res = await chai.request(myserver).get('/api/v1/role/'+testrole);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name').eql("nuevorolEDIT");
            res.body.should.have.property('permissions').eql(5);
            res.body.should.have.property('icon').eql("fas fa-user");
            res.body.should.have.property('_id');
        });



    });


    describe('/DELETE Roles', () => {


        it('Delete without token', async () => {
            let res = await chai.request(myserver).delete('/api/v1/role/'+testrole);

            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("Tu petición no tiene cabecera de autorización");
        });


        it('Delete used role', async () => {
            let res = await chai.request(myserver).delete('/api/v1/role/'+dummy.role).set("Authorization",admin.token);
            res.should.have.status(422);
        });

        it('Delete test role', async () => {
            let res = await chai.request(myserver).delete('/api/v1/role/'+testrole).set("Authorization",admin.token);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('name').eql("nuevorolEDIT");
            res.body.should.have.property('permissions').eql(5);
            res.body.should.have.property('icon').eql("fas fa-user");
            res.body.should.have.property('_id');
        });

        it('Get removed role', async () => {
            let res = await chai.request(myserver).get('/api/v1/role/'+testrole);
            res.should.have.status(404);
        });

    });

});