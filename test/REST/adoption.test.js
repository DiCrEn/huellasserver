'use strict'

const {ROLE_LIST} = require("../../utils/roleutils");

let adoptionadmin="";
let adoptionadmin2="";

describe('Adoptions', () => {

    before(async () => {

    });

    describe('/POST Add new adoption', () => {

        it('New adoption without token', async () => {
            let res = await chai.request(myserver).post('/api/v1/adoption/')
                .send({
                    name:"winky",
                    text:"gata blanca muy revelde",
                    animal:"gato",
                    sex:"F",
                    media: ["img1","img2"]
                });

            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql("Tu petición no tiene cabecera de autorización");
        });

        it('New adoption from dummy', async () => {
            let res = await chai.request(myserver).post('/api/v1/adoption/')
                .set("Authorization",dummy.token)
                .send({
                    name:"winky",
                    text:"gata blanca muy revelde",
                    animal:"gato",
                    sex:"F",
                    media: ["img1","img2"]
                });

            res.should.have.status(403);
        });

        it('New adoption from admin', async () => {
            let res = await chai.request(myserver).post('/api/v1/adoption/')
                .set("Authorization",admin.token)
                .send({
                    name:"winky",
                    text:"gata blanca muy revelde",
                    animal:"gato",
                    sex:"F",
                    media: ["img1","img2"]
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user').eql(admin.id);
            res.body.should.have.property('name').eql("winky");
            res.body.should.have.property('media').be.a('array');
            adoptionadmin = res.body._id;
        });

        it('Test sanitize-html', async () => {
            let res = await chai.request(myserver).post('/api/v1/adoption/')
                .set("Authorization",admin.token)
                .send({
                        name:"winky",
                        text:`¡Romper internet!<script>searchInGoogle('google');</script>`,
                        animal:"gato",
                        sex:"F",
                        media: ["img1","img2"]

                    });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('user').eql(admin.id);
            res.body.should.have.property('name').eql("winky");
            res.body.should.have.property('text').eql("¡Romper internet!");
            res.body.should.have.property('media').be.a('array');
            adoptionadmin2 = res.body._id;
        });



    });

    describe('/GET adoption', () => {

        it('Get adoption list', async () => {
            let res = await chai.request(myserver).get('/api/v1/adoption/');

            res.should.have.status(200);
            res.body.should.to.have.length(2);
            res.body.should.be.a('array');
            res.body.every(i => {
                expect(i).to.have.all.keys(
                    '__v',
                    '_id',
                    'date',
                    'media',
                    'name',
                    "adopted",
                    'text',
                    'animal',
                    'sex',
                    'user');
            });
        });

        it('Get adoption', async () => {
            let res = await chai.request(myserver).get('/api/v1/adoption/'+adoptionadmin);

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.to.have.all.keys(
                '__v',
                '_id',
                'date',
                'media',
                "adopted",
                'name',
                'text',
                'animal',
                'sex',
                'user');

        });


        it('Get adoption not exist', async () => {
            let res = await chai.request(myserver).get('/api/v1/adoption/'+'333');
            res.should.have.status(404);
        });


    });

    describe('/PUT adoption', () => {

        it("Dummy edit admin's adoption", async () => {
            let res = await chai.request(myserver).put('/api/v1/adoption/'+adoptionadmin)
                .set("Authorization",dummy.token)
                .send({
                    name:"winky",
                    text:"gata blanca muy peor que muy revelde",
                    animal:"gato",
                    sex:"F",
                    media: ["img1","img2"]
                });
            res.should.have.status(403);
        });

        it('Admin edit admin\'s adoption', async () => {
            let res = await chai.request(myserver).put('/api/v1/adoption/'+adoptionadmin)
                .set("Authorization",admin.token)
                .send({
                    name:"winky",
                    text:"gata blanca muy peor que muy revelde",
                    animal:"gato",
                    sex:"F",
                    media: ["img1","img2"]
                });

            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('text').eql("gata blanca muy peor que muy revelde");
            res.body.should.have.property('name').eql("winky");
            res.body.should.have.property('media').be.a('array');

        });


    });

    describe('/DELETE adoption', () => {

        it('Dummy delete admin\'s adoption', async () => {
            let res = await chai.request(myserver).delete('/api/v1/adoption/'+adoptionadmin)
                .set("Authorization",dummy.token).send();
            res.should.have.status(403);
        });

        it('Admin delete admin\'s adoption', async () => {
            let res = await chai.request(myserver).delete('/api/v1/adoption/'+adoptionadmin)
                .set("Authorization",admin.token).send();
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('_id');
            res.body.should.have.property('date');
            res.body.should.have.property('text').eql("gata blanca muy peor que muy revelde");
            res.body.should.have.property('name').eql("winky");
            res.body.should.have.property('media').be.a('array');
        });

        it('Admin delete admin\'s adoption again', async () => {
            let res = await chai.request(myserver).delete('/api/v1/adoption/'+adoptionadmin)
                .set("Authorization",admin.token).send();
            res.should.have.status(404);
        });


    });

});