module.exports = {
    apps : [
        {
            name: "huellasServer",
            script: "index.js",
            watch: true,
            ignore_watch : ["node_modules", "uploads"],
            instance_var: 'INSTANCE_ID',
            env: {
                "PORT": 8088,
                "NODE_ENV": "production"
            }
        }
    ]
};