'use strict'

const express = require('express');
const RoleController = require('../controllers/role');
const auth = require('../middlewares/auth');
const permissions = require('../middlewares/permissions');
const validations = require('../middlewares/validations');
const {ROLE_LIST} = require("../utils/roleutils");

const router = express.Router();

router.post('/',
    auth.ensureAuthenticated,
    permissions.checkPermissions(ROLE_LIST.role.w),
    RoleController.insertRole);

router.get('/',
    RoleController.getRoles);

router.get('/:id',
    RoleController.getRoles);

router.put('/:id',
    auth.ensureAuthenticated,
    permissions.checkPermissions(ROLE_LIST.role.w),
    RoleController.editRole);

router.delete('/:id',
    auth.ensureAuthenticated,
    permissions.checkPermissions(ROLE_LIST.role.w),
    RoleController.deleteRole
    );


module.exports = router;