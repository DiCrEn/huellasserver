'use strict'

const article = require('../controllers/article');
const express = require('express');
const UserController = require('../controllers/user');
const auth = require('../middlewares/auth');
const permissions = require('../middlewares/permissions');
const validations = require('../middlewares/validations');
const {ROLE_LIST} = require("../utils/roleutils");


const router = express.Router();


router.post('/',
    auth.ensureAuthenticated,
    permissions.checkPermissions(ROLE_LIST.article.w),
    article.newArticle);

router.get('/', article.getArticle);

router.get('/tags',article.getTags);

router.get('/:id', article.getArticle);

router.put('/:id',
    auth.ensureAuthenticated,
    validations.articleexist,
    permissions.articleWritePermitted,
    article.editArticle);

router.delete('/:id',
    auth.ensureAuthenticated,
    validations.articleexist,
    permissions.articleWritePermitted,
    article.deleteArticle);


module.exports = router;