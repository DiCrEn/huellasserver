'use strict'

const AdoptionController = require('../controllers/adoption');
const express = require('express');
const auth = require('../middlewares/auth');
const permissions = require('../middlewares/permissions');
const validations = require('../middlewares/validations');
const {ROLE_LIST} = require("../utils/roleutils");

const multipart = require('connect-multiparty');
const md_upload_user_image = multipart({
    uploadDir: './uploads/adoptions/',
});

const router = express.Router();

router.post('/',
    auth.ensureAuthenticated,
    permissions.checkPermissions(ROLE_LIST.adoption.w),
    AdoptionController.newAdoption);

router.get('/', AdoptionController.getAdoption);

router.get('/:id', AdoptionController.getAdoption);

router.put('/adopt/:id',
    validations.adoptionexist,
    AdoptionController.adopt);

router.put('/:id',
    auth.ensureAuthenticated,
    validations.adoptionexist,
    permissions.adoptionWritePermitted,
    AdoptionController.editAdoption);

router.delete('/:id',
    auth.ensureAuthenticated,
    validations.adoptionexist,
    permissions.adoptionWritePermitted,
    AdoptionController.deleteAdoption);

router.post('/image/',
    auth.ensureAuthenticated,
    md_upload_user_image,
    AdoptionController.uploadImage);

router.delete('/image/:id',
    auth.ensureAuthenticated,
    AdoptionController.deleteImage);


router.get("/image/:id",
    AdoptionController.getImage);



module.exports = router;