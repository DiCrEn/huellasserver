'use strict'

const comment = require('../controllers/comment');
const express = require('express');
const auth = require('../middlewares/auth');
const permissions = require('../middlewares/permissions');
const validations = require('../middlewares/validations');
const {ROLE_LIST} = require("../utils/roleutils");

const router = express.Router();

router.post('/',
    auth.ensureAuthenticated,
    permissions.checkPermissions(ROLE_LIST.comment.w),
    comment.newComment);

router.get('/',
    comment.getComment);

router.get('/:id',
    comment.getComment);

router.put('/:id',
    auth.ensureAuthenticated,
    validations.commentexist,
    permissions.commentWritePermitted,
    comment.editComment);

router.put('/:id/vote',
    auth.ensureAuthenticated,
    validations.commentexist,
    permissions.checkPermissions(ROLE_LIST.comment.w),
    comment.voteComment
    );

router.delete('/:id',
    auth.ensureAuthenticated,
    validations.commentexist,
    permissions.commentWritePermitted,
    comment.deleteComment);


module.exports = router;