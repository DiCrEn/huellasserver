'use strict'

const express = require('express');
const auth = require('../controllers/auth');


const router = express.Router();


router.get('/facebook', auth.facebook);
router.post('/facebook', auth.facebook);


module.exports = router;