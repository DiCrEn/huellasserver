'use strict'

const express = require('express');
const UserController = require('../controllers/user');
const auth = require('../middlewares/auth');
const permissions = require('../middlewares/permissions');
const validations = require('../middlewares/validations');

const multipart = require('connect-multiparty');
const md_upload_user_image = multipart({
    uploadDir: './uploads/users/avatars',
});

const {ROLE_LIST} = require("../utils/roleutils");

const router = express.Router();

router.post('/',
    auth.maybeAuthenticated,
    validations.validemail,
    validations.emailexist,
    UserController.insertUser);

router.post('/login',
    validations.validemail,
    UserController.login);

router.get('/',
    auth.maybeAuthenticated,
    UserController.getUsers);

router.get('/:id',
    auth.maybeAuthenticated,
    UserController.getUsers);

router.put('/:id',
    auth.ensureAuthenticated,
    permissions.userWritePermitted,
    validations.validemail,
    validations.emailexist,
    UserController.editUser);

router.delete('/:id',
    auth.ensureAuthenticated,
    permissions.userWritePermitted,
    UserController.deleteUser
    );

router.post("/avatar/:id",
    auth.ensureAuthenticated,
    permissions.userWritePermitted,
    md_upload_user_image,
    UserController.setAvatar);

router.delete("/avatar/:id",
    auth.ensureAuthenticated,
    permissions.userWritePermitted,
    UserController.deleteAvatar);

router.get("/avatar/:id",
    UserController.getAvatar);

module.exports = router;