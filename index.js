'use strict'

const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const router = require('./route.js');
const roleutils = require('./utils/roleutils');
const Role = require('./models/role');
const User = require('./models/user');
const config = require('config');
const encryptutils = require("./utils/encryptutils");
const morgan = require('morgan');
const https = require('https');
const fs = require('fs');

//En producción con pm2 le indicamos el puerto, para que no haga conflicto con los tests
const port = process.env.PORT || 8888;

const basicUserPermissions = roleutils.ROLE_LIST.comment.w;

//Cuando mongodb esté preparado comprobamos la base de datos.
mongoose.connect(config.get("DBHOST"),(err,res) => {
    if(err){
        throw err;
    }else{
        //Comprobar si existe el role de administrador y si no crearlo, así como el usuario.
        Role.findOne({special:1} , (error,role) => {
            if (error) {
                console.log(error);
                process.exit(1);
            }else{
                let save = false;
                if(!role){
                    role = new Role();
                    role.name = "Admin";
                    role.permissions = roleutils.getRoleAdmin();
                    role.icon = "fas fa-star";
                    role.special=1;
                    save = true;
                }else if(role.permissions !== roleutils.getRoleAdmin()){
                    //Si se han añadido nuevos permisos actualizo el role de admin.
                    role.permissions = roleutils.getRoleAdmin();
                    save = true;
                }else{
                    createDefaultAdminUser(role);
                }

                if(save){
                    role.save((error, roleStored) => {
                        if (error) {
                            console.log(error);
                            process.exit(1);
                        }else if (!roleStored) {
                                console.log("No se ha podido generar el role de administrador");
                                process.exit(1);
                        } else {
                            createDefaultAdminUser(roleStored);
                        }
                    });
                }
            }
        });

        //Crear si no existe el role de usuario registrado básico.
        Role.findOne({special:2} , (error,role) => {
            if (error) {
                console.log(error);
                process.exit(1);
            }else if(!role){
                role = new Role();
                role.name = "User";
                role.icon = "fas fa-user";
                role.special = 2;
                role.permissions = basicUserPermissions; //Usuario registrado, sin permisos extra
                role.save((error, roleStored) => {
                    if (error) {
                        console.log(error);
                        process.exit(1);
                    }else if (!roleStored) {
                        console.log("No se ha podido generar el role de usuario");
                        process.exit(1);
                    }
                });
            }
        });

    }
});

function createDefaultAdminUser(roleStored){

    //Crear el usuario administrador si no existe ninguno.
    User.findOne({"role": roleStored._id}, (error,user) => {
        if(error){
            console.log(error);
            process.exit(1);
        }else if(!user){
            //No existe ningún admin, meter el de por defecto
            const adminuser = new User();
            adminuser.name="admin";
            adminuser.lastname="admin";
            adminuser.email=config.get("ADMIN_EMAIL");
            adminuser.password=encryptutils.encryptpass(config.get("ADMIN_PASSWORD"));
            adminuser.role = roleStored._id;

            adminuser.save((error,userStored) => {
                if (error) {
                    console.log(error);
                    process.exit(1);
                }else if(!userStored) {
                    console.log("No se ha podido generar usuario administrador");
                    process.exit(1);
                }else{
                    //Arrancar el servidor de express cuando mongodb funcine
                    applisten();
                }

            });
        }else{
            //Arrancar el servidor de express cuando mongodb funcine
            applisten();
        }
    });
}


//mongoose.set('debug', true);


//Crear el servidor de node
const app = express();

/*
//Forzar timeoout para probar la interfaz
app.use(function(req,res,next){
    setTimeout(next,800);
});*/

if(config.util.getEnv('NODE_ENV') !== 'test') app.use(morgan('combined'));

//Configuración CORS
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,cache-control');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(bodyparser.json({limit: '50mb'}));

if(config.util.getEnv('NODE_ENV')  === 'dev' || config.util.getEnv('NODE_ENV')  === 'test') {
    app.use('/api/v1/', router);
}else{ //En producción hacemos proxy con apache, por lo que es apache el que ya indica la ruta /api/ para acceder.
    app.use('/v1/', router);
}

//middleware para los errores
app.use((req, res, next) => {
    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    let err = new Error('No encontrado ' + fullUrl);
    err.status = 404;
    next(err);
});

//Este middleware recibe los errores con next(err)
app.use((err, req, res, next) => {
    let respuesta = {};
    respuesta['message'] = err.message;
    respuesta['error'] = err;
    //Si es un error propio del servidor err.status esta undefined,
    //a si que usamos || 500 para que use el 500 por defecto
    res.status(err.status || 500).json(respuesta);
});

function applisten() {

    if(config.util.getEnv('NODE_ENV')  === 'dev' || config.util.getEnv('NODE_ENV')  === 'test') {
        /* For testing init*/
        app.listen(port, function () {
            app.emit("app_started");
        });
    }else{
        const credentials = {
            key: fs.readFileSync('/etc/letsencrypt/live/dicren.com/privkey.pem'),
            cert: fs.readFileSync('/etc/letsencrypt/live/dicren.com/cert.pem'),
            ca: fs.readFileSync('/etc/letsencrypt/live/dicren.com/chain.pem')
        };

        https.createServer(credentials, app)
            .listen(port, function () {
                app.emit("app_started");
            });
    }
}

module.exports = app; // for testing